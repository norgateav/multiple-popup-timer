/*******************************************************************************************
  SIMPL+ Module Information
  (Fill in comments below)
*******************************************************************************************/
/*
Dealer Name:
System Name:
System Number:
Programmer:
Comments:

Copyright � 2017 Norgate AV Holdings Ltd

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>
*/

/*******************************************************************************************
  Compiler Directives
  (Uncomment and declare compiler directives as needed)
*******************************************************************************************/
// #ENABLE_DYNAMIC
#SYMBOL_NAME "Multiple Popup Timer"
// #HINT ""
#DEFINE_CONSTANT	MAX_POPUPS	32
#CATEGORY "46" "UI" 
// #PRINT_TO_TRACE
#DIGITAL_EXPAND inputswithoutputs
// #ANALOG_SERIAL_EXPAND 
// #OUTPUT_SHIFT 
// #HELP_PDF_FILE ""
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
#ENCODING_ASCII
// #ENCODING_UTF16
// #ENCODING_INHERIT_FROM_PARENT
// #ENCODING_INHERIT_FROM_PROGRAM
/*
#HELP_BEGIN
   (add additional lines of help lines)
#HELP_END
*/

/*******************************************************************************************
  Include Libraries
  (Uncomment and include additional libraries as needed)
*******************************************************************************************/
// #CRESTRON_LIBRARY ""
// #USER_LIBRARY ""

/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
DIGITAL_INPUT	_skip_,Clear,Panel_Activity,Popup[MAX_POPUPS]; 
// ANALOG_INPUT 
// STRING_INPUT 
// BUFFER_INPUT 

DIGITAL_OUTPUT	_skip_,_skip_,_skip_,Popup_Fb[MAX_POPUPS];	
// ANALOG_OUTPUT 
// STRING_OUTPUT 

/*******************************************************************************************
  SOCKETS
  (Uncomment and define socket definitions as needed)
*******************************************************************************************/
// TCP_CLIENT
// TCP_SERVER
// UDP_SOCKET

/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
INTEGER_PARAMETER	Time_Out;
// SIGNED_INTEGER_PARAMETER
// LONG_INTEGER_PARAMETER
// SIGNED_LONG_INTEGER_PARAMETER
// STRING_PARAMETER

/*******************************************************************************************
  Parameter Properties
  (Uncomment and declare parameter properties as needed)
*******************************************************************************************/
#BEGIN_PARAMETER_PROPERTIES Time_Out
   propValidUnits = unitTime;
   propDefaultUnit = unitTime;
   // propBounds = lower_bound , upper_bound;
   propDefaultValue = 20s;  // or, propDefaultValue = "";
   // propList = // { "value" , "label" } , { "value" , "label" } , ... ;
   // propShortDescription = "status_bar_hint_text";
   // #BEGIN_PROP_FULL_DESCRIPTION  line_1...  line_2...  line_n  #END_PROP_FULL_DESCRIPTION
   // #BEGIN_PROP_NOTES line_1...  line_2...  line_n  #END_PROP_NOTES
#END_PARAMETER_PROPERTIES

/*******************************************************************************************
  Structure Definitions
  (Uncomment and define structure definitions as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: struct.myString = "";
*******************************************************************************************/
/*
STRUCTURE MyStruct1
{
};

MyStruct1 struct;
*/

/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
volatile INTEGER x,iInUse;
// LONG_INTEGER
// SIGNED_INTEGER
// SIGNED_LONG_INTEGER
// STRING

/*******************************************************************************************
  Functions
  (Add any additional functions here)
  Note:  Functions must be physically placed before the location in
         the code that calls them.
*******************************************************************************************/
/*
Function MyFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here
}
*/

/*
Integer_Function MyIntFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here

    Return (0);
}
*/

/*
String_Function MyStrFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here

    Return ("");
}
*/

/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/
release popup {
	integer i;
	x = getlastmodifiedarrayindex();
	if(!popup_fb[x]) {
		cancelallwait();
		for(i = 1 to iInUse) { popup_fb[i] = 0; }
    	popup_fb[x] = 1;
		wait(time_out,time_out_time) popup_fb[x] = 0; 	
	} else {
    	//Turn it off
		cancelallwait();
		popup_fb[x] = 0;			
	}
}

push popup
push panel_activity {
	if(x) {
		if(popup_fb[x]) {
   			retimewait(time_out,time_out_time);
		}
	}
}

push clear {
	integer i;
	cancelallwait();
	for(i = 1 to iInUse) { popup_fb[i] = 0; }
}


/*
PUSH input
{
    // TODO:  Add code here
}
*/

/*
RELEASE input
{
    // TODO:  Add code here
}
*/

/*
CHANGE input
{
    // TODO:  Add code here
}
*/

/*
EVENT
{
    // TODO:  Add code here
}
*/

/*
SOCKETCONNECT
{
    // TODO:  Add code here
}
*/

/*
SOCKETDISCONNECT
{
    // TODO:  Add code here
}
*/

/*
SOCKETRECEIVE
{
    // TODO:  Add code here
}
*/

/*
SOCKETSTATUS
{
    // TODO:  Add code here
}
*/

/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
Function Main(){
	integer i;
	waitforinitializationcomplete();
	x = 0;
	for(i = MAX_POPUPS to 1 step - 1) {
		if(issignaldefined(popup_fb[i])) {
			iInUse = i;
			break;
		}
	}
}
